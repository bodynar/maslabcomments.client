/** Delay before notification will be hiden */
export const NotificationHideDelay: number = 2.5 * 1000;

/** Amount of notification to show "Dismiss all" button */
export const NotificationCountToShowHideAll = 2;
