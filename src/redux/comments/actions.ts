/** Save loaded comments into state */
export const setComments = "comments/setComments";

/** Increment appearance count of specified comment action type */
export const increment = "comments/increment";

/** Add new comment action type */
export const addComment = "comments/addComment";

/** Update specified comment action type*/
export const updateComment = "comments/updateComment";

/** Delete specified comment action type */
export const deleteComment = "comments/deleteComment";

/** Set comments module state action type */
export const setModuleState = "comments/setModuleState";
